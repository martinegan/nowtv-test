describe('Controller: MainController as main', function () {
    var MainController, scope, $log, $location, $httpBackend;

    beforeEach(module('icebergMovies'));
    beforeEach(inject(function ($controller, $rootScope, _$log_, _$location_, _$httpBackend_) {
        scope = $rootScope.$new();
        $log = _$log_;
        $location = _$location_;
        $httpBackend = _$httpBackend_;

        createController = function(){
            return $controller('MainController', {
                $scope: scope,
                $log: $log
            })
        }
    }));

    it('should exist', function () {
        MainController = createController();
        expect(!!MainController).toBe(true);
    });

    it('/api/movies should be called', function(){
        MainController = createController();
        var url = '/api/movies?end=20&pageNumber=1&pageSize=20&sortBy=title&start=1';
        var httpResponse = {
            "content": [{
                "title": "2 Days in the Valley",
                "directors": {
                    "list": [{
                        "name": "John Herzfeld"
                    }]
                },
                "actors": {
                    "list": [{
                        "name": "James Spader"
                    }]
                },
                "duration": 6000,
                "rating": 3,
                "year": 1996
            }],
            "pageNumber":1,
            "totalPages":1,
            "matchedItems":1,
            "totalItems":1,
            "startItem":1,
            "endItem":1
        };
        $httpBackend.expectGET(url).respond(200, httpResponse);
        $httpBackend.flush();

        expect(MainController.movies.length).toBe(1);
        expect(MainController.movies.toString()).toEqual(httpResponse.toString());
    })

    it('searchOptions should exist and be set to default values', function(){
        MainController = createController();
        var expectedDefaults = {
            pageNumber: 1,
            pageSize: 20,
            start: 1,
            end: 20,
            sortBy: 'title',
            searchTerm: null
        };

        expect(MainController.searchOptions).toBeDefined();
        expect(MainController.searchOptions).toEqual(expectedDefaults);
    })

    it('getTimes(5) should return an error with 5 empty elements', function(){
        MainController = createController();
        var result = MainController.getTimes(5);
        expect(result.length).toBe(5);
    })

    it('getActorList() should return a comma seperated list of actors when passed an actors array', function(){
        MainController = createController();

        var actors = [{
            "name": "James Spader"
        }, {
            "name": "Danny Aiello"
        }, {
            "name": "Eric Stoltz"
        }, {
            "name": "Teri Hatcher"
        }, {
            "name": "Glenne Headly"
        }];

        var expectedList = "James Spader,Danny Aiello,Eric Stoltz,Teri Hatcher,Glenne Headly";

        expect(MainController.getActorList(actors)).toBe(expectedList);
    })

    it('changing vm.searchString to "a" triggers watch and updates vm.searchHint to show message', function(){
        var url = '/api/movies?end=20&pageNumber=1&pageSize=20&sortBy=title&start=1';
        var httpResponse = {
            "content": [{
                "title": "2 Days in the Valley",
                "directors": {
                    "list": [{
                        "name": "John Herzfeld"
                    }]
                },
                "actors": {
                    "list": [{
                        "name": "James Spader"
                    }]
                },
                "duration": 6000,
                "rating": 3,
                "year": 1996
            }],
            "pageNumber":1,
            "totalPages":1,
            "matchedItems":1,
            "totalItems":1,
            "startItem":1,
            "endItem":1
        };

        $httpBackend.expectGET(url).respond(200, httpResponse);

        MainController = createController();
        scope.searchString = 'a';
        scope.$digest();

        $httpBackend.flush();

        expect(MainController.searchHint).toBe('Enter at least three characters to begin search');
    })

    it('changing vm.searchString to "aaa" triggers watch and updates vm.searchHint to an empty string', function(){
        var url = '/api/movies?end=20&pageNumber=1&pageSize=20&sortBy=title&start=1';
        var url2 = '/api/movies?end=20&pageNumber=1&pageSize=20&searchTerm=aaa&sortBy=title&start=1';

        var httpResponse = {
            "content": [{
                "title": "2 Days in the Valley",
                "directors": {
                    "list": [{
                        "name": "John Herzfeld"
                    }]
                },
                "actors": {
                    "list": [{
                        "name": "James Spader"
                    }]
                },
                "duration": 6000,
                "rating": 3,
                "year": 1996
            }],
            "pageNumber":1,
            "totalPages":1,
            "matchedItems":1,
            "totalItems":1,
            "startItem":1,
            "endItem":1
        };

        $httpBackend.expectGET(url).respond(200, httpResponse);
        $httpBackend.expectGET(url2).respond(200, httpResponse);

        MainController = createController();
        scope.searchString = 'aaa';
        scope.$digest();

        $httpBackend.flush();

        expect(MainController.searchHint).toBe('');
        expect(MainController.searchOptions.searchTerm).toBeDefined();
        expect(MainController.searchOptions.searchTerm).toBe(scope.searchString);
    })

    it('getResultsPage(2)', function(){
        var url = '/api/movies?end=20&pageNumber=1&pageSize=20&sortBy=title&start=1';
        var httpResponse = {
            "content": [{
                "title": "2 Days in the Valley",
                "directors": {
                    "list": [{
                        "name": "John Herzfeld"
                    }]
                },
                "actors": {
                    "list": [{
                        "name": "James Spader"
                    }]
                },
                "duration": 6000,
                "rating": 3,
                "year": 1996
            }],
            "pageNumber":1,
            "totalPages":1,
            "matchedItems":1,
            "totalItems":1,
            "startItem":1,
            "endItem":1
        };

        $httpBackend.expectGET(url).respond(200, httpResponse);

        MainController = createController();

        spyOn(MainController, 'getResultPage').and.callThrough();

        MainController.movies = {
            totalPages: 2
        };

        MainController.getResultPage(2);

        expect(MainController.getResultPage).toHaveBeenCalledWith(2);
        expect(MainController.searchOptions.pageNumber).toBe(2);
    })
});
